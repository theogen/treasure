extends Control


var _loc_key


func _ready():
	pass


func display(key, delay = 1):
	_loc_key = key
	$timer.start(delay)


func _on_timer_timeout():
	$text.text = tr(_loc_key)
	$anim.play("fade")
