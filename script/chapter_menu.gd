extends Control


onready var game = get_node("/root/root")

var scene_to_load
var ignore_input = false
var _fade_signal_connected = false

func _ready():
	pass
	
	
func focus(ignore_input = true):
	get_node("buttons/back").grab_focus()
	self.ignore_input = ignore_input

func _process(_delta):
	if not visible:
		return
	if Input.is_action_just_released("ui_accept"):
		ignore_input = false
	if Input.is_action_just_pressed("ui_accept") and not ignore_input:
		_handle_button(get_focus_owner().name)


func _handle_button(button):
	if button == "back":
		get_node("../main").focus(true)
		get_node("../main").visible = true
		visible = false
		return
	visible = false
	if not _fade_signal_connected:
		game.camera.connect("on_fade_finish", self, "_on_fade_finish")
		_fade_signal_connected = true
	scene_to_load = button
	game.camera.fadein("chapters")


func _on_fade_finish(fade_name):
	if fade_name != "chapters":
		return
	game.load_scene(scene_to_load, true)
	if scene_to_load == "intro":
		game.play()
	else:
		game.start()
