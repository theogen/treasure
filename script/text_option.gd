extends "res://script/text_button.gd"

var _value

func _ready():
	pass

func update_text():
	text = "%s: %s" % [get_text(), _value]
	if focused:
		text = "[ %s ]" % text

func set_value(value):
	_value = value
	update_text()
