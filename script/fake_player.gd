extends AnimatedSprite


export(Vector2) var move_to

const SPEED = 30

var moving = true


func _ready():
	pass


func _process(delta):
	position = position.move_toward(move_to, delta * SPEED)
	if moving and position == move_to:
		moving = false
		play("idle")
		get_node("../end_timer").start()
