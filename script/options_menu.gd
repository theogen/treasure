extends Control


signal on_language_update


var ignore_input = false

onready var language_text = get_node("buttons/language")
onready var music_text = get_node("buttons/music")
onready var sound_text = get_node("buttons/sound")
onready var environ_text = get_node("buttons/environment")

const LANGUAGES = [ "en", "ru" ]

var _language = 0
var _music = 50
var _sound = 100
var _environment = 100

func _ready():
	language_text.set_value(tr("language"))
	music_text.set_value("50%")
	sound_text.set_value("100%")
	environ_text.set_value("100%")


func focus():
	get_node("buttons/back").grab_focus()
	ignore_input = true


func _process(_delta):
	if not visible:
		return
	if Input.is_action_just_released("ui_accept"):
		ignore_input = false
	if Input.is_action_just_pressed("ui_accept") and not ignore_input:
		_handle_button(get_focus_owner().name)


func back():
	var path = "../main"
	if get_node("/root/root").paused:
		path = "../pause"
	get_node(path).focus()
	get_node(path).visible = true
	visible = false


func set_language(lang):
	_language = LANGUAGES.bsearch(lang)
	TranslationServer.set_locale(lang)
	language_text.set_value(tr("language"))
	emit_signal("on_language_update")


func _increment_volume(bus, volume) -> int:
	volume += 10
	if volume > 100:
		volume = 0
	var volumedb = linear2db(float(volume) / 100)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index(bus), volumedb)
	return volume


func _handle_button(button):
	if button == "language":
		_language += 1
		if _language == len(LANGUAGES):
			_language = 0
		set_language(LANGUAGES[_language])
	if button == "music":
		_music = _increment_volume("Music", _music)
		music_text.set_value("%d%%" % _music)
	if button == "sound":
		_sound = _increment_volume("Sound", _sound)
		sound_text.set_value("%d%%" % _sound)
	if button == "environment":
		_environment = _increment_volume("Environment", _environment)
		environ_text.set_value("%d%%" % _environment)
	if button == "back":
		back()
