extends "res://script/platform.gd"

export var min_height = 1

export var max_height = 10

var movement_speed = 10

var initial_height
var height

var swap_directions = false

var move
var moving = false

var ground_y

onready var _shape = get_node("StaticBody2D/CollisionShape2D")
onready var _sound = get_node("sound")
onready var _sound_stop = get_node("sound_stop")

func bury():
	if height == min_height:
		return
	set_height(height - 1)


func elevate():
	if height == max_height:
		return
	set_height(height + 1)


func set_height(h):
	if height == h:
		return
	height = h
	move.y = _height_to_y(h)
	if not _sound.playing:
		_sound.play()


func _ready():
	height = _y_to_height(position.y)
	initial_height = height
	ground_y = get_parent().position.y
	move = position
	connect("on_platform_exit", self, "_on_platform_exit")
	_randomize_dust()


func _process(delta):
	position = position.move_toward(move, movement_speed * delta)
	var moving = position != move
	var stopped = self.moving and !moving
	self.moving = moving
	if moving:
		$dustl.visible = true
		$dustr.visible = true
		$dustl.global_position.y = ground_y - 4
		$dustr.global_position.y = ground_y - 4
	elif stopped:
		$dustl.visible = false
		$dustr.visible = false
		_sound.stop()
		_sound_stop.play()
	if playerInside:
		player.snap = moving
	_enable_depending_on_player_level()


func _enable_depending_on_player_level():
	if player.position.y + 7 <= global_position.y:
		_shape.set_deferred("disabled", false)
	else:
		_shape.set_deferred("disabled", true)


func _randomize_dust():
	$dustl.frame = randi() % $dustl.frames.get_frame_count("default")
	$dustr.frame = $dustl.frame


func _on_platform_exit():
	player.snap = false


func _on_player_jump():
	player.snap = false
	._on_player_jump()


func _y_to_height(y):
	return -y / 8


func _height_to_y(h):
	return -h * 8
