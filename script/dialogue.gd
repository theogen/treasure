extends Area2D


signal on_dialogue_end
signal on_choice
signal on_dialogue_proceed


export var dialogue_index = 1
export(Color) var background_color = Color("adb4c0")
export(Color) var interlocutor_color = Color("6e463f")
export(Color) var player_color = Color("404f6c")
export var fullscreen = false


const PRINTING_SPEED = 20


onready var dialogue = get_node("/root/root/gui/dialogue")
onready var dialogue_text = dialogue.get_node("text")
onready var dialogue_print_sound = dialogue.get_node("print")
onready var dialogue_choices = dialogue.get_node("choices")

onready var game = get_node("/root/root")
onready var player = get_node("/root/root/player")


var dialogue_started = false

var _keys
var _index = 0
var _branch = 1

# Whether choice mode is on
var _choice = false
# Currenlty selected choice
var _selected_choice = 0
# Array of current choices
var _choices = []

var _visible_characters = 0

var ignore_input = false
var _end_keyword = false

class KeyProps:
	var dialogue_id = 0
	var branch
	var type
	var id
	var goto = 0
	var gender


func start_dialogue():
	# Setting colors
	dialogue.get_node("background").modulate = background_color
	dialogue_text.modulate = interlocutor_color
	for i in range(dialogue_choices.get_child_count()):
		dialogue_choices.get_child(i).modulate = player_color
	# Fullscreen
	if fullscreen:
		dialogue.anchor_bottom = 1
		dialogue.margin_left = 0
		dialogue.margin_right = 0
		dialogue.margin_top = 0
	else:
		dialogue.anchor_bottom = 0
		dialogue.margin_left = 40
		dialogue.margin_right = -40
		dialogue.margin_top = 20
	player.can_move = false
	dialogue.visible = true
	dialogue_text.visible = true
	dialogue_choices.visible = false
	dialogue_started = true
	_index = 0
	_branch = 1
	_choice = false
	_selected_choice = 0
	_choices = []
	proceed()


func end_dialogue():
	player.can_move = true
	dialogue.visible = false
	dialogue_started = false
	emit_signal("on_dialogue_end", dialogue_index)
	monitoring = false


func set_text(key):
	dialogue_text.text = tr(key)


func proceed():
	if _index == len(_keys) or _end_keyword:
		end_dialogue()
		return
	var key = _keys[_index]
	var props = _parse_key(key)
	# Ignoring dialogue for the opposite gender
	if props.gender and props.gender != player.gender:
		_index += 1
		proceed()
		return
	# Entering choice mode
	if props.type == "r":
		_choice_mode()
		return
	set_text(_keys[_index])
	_index += 1
	if props.goto > 0:
		_goto(props.goto)
	emit_signal("on_dialogue_proceed", key)
	if "END" in key:
		_end_keyword = true
	_visible_characters = 0
	dialogue_text.visible_characters = 0


func _ready():
	_load_keys()
	get_node("/root/root/gui/options").connect("on_language_update", self, "_on_language_update")
	connect("on_dialogue_end", game, "_on_dialogue_end")
	connect("on_dialogue_proceed", game, "_on_dialogue_proceed")
	connect("on_choice", game, "_on_dialogue_choice")
	game.connect("on_unpause", self, "_on_unpause")

func _load_keys():
	var loc = TranslationServer.get_locale().split("_")[0]
	_keys = _get_keys_from_loc_file("res://loc/%s.%s.translation" % [loc, loc], dialogue_index)

func _on_language_update():
#	var key = _keys[_index - 1]
#	var props1 = _parse_key(key)
	_load_keys()
	if not dialogue_started:
		return
#	var i = -1
#	for k in _keys:
#		i += 1 
#		var props2 = _parse_key(k)
#		if props1.branch == props2.branch and \
#			props1.type == props2.type and \
#			props1.id == props2.id:
#				if props1.gender == props2.gender:
#					_index = i
#					break
#				if not props1.gender and props2.gender:
#					if props2.gender != player.gender:
#						continue
#					else:
#						_index = i
#						break
#				if not props2.gender and props1.gender:
#					_index = i
#					break
#	_index += 1
	if not _choice:
		set_text(tr(_keys[_index - 1]))
	else:
		_update_choices_text()


func _on_unpause():
	if dialogue_started:
		ignore_input = true


func _process(delta):
	if not dialogue_started or game.paused:
		return
	var total = dialogue_text.get_total_character_count()
	if dialogue_text.visible_characters < total:
		_visible_characters += delta * PRINTING_SPEED
		if _visible_characters > dialogue_text.visible_characters:
			if dialogue_text.visible_characters % 2 == 1:
				dialogue_print_sound.play()
		dialogue_text.visible_characters = _visible_characters
	if Input.is_action_just_released("ui_accept"):
		ignore_input = false
	elif ignore_input:
		return
	if Input.is_action_just_pressed("ui_accept"):
		var v = dialogue_text.visible_characters
		if v < total:
			dialogue_text.visible_characters = total
			return
		if _choice:
			_choose()
		proceed()
	if Input.is_action_just_pressed("ui_down"):
		if _selected_choice + 1 < len(_choices):
			_selected_choice += 1
			_update_choices_text()
	if Input.is_action_just_pressed("ui_up"):
		if _selected_choice - 1 >= 0:
			_selected_choice -= 1
			_update_choices_text()



func _goto(branch):
	for i in range(len(_keys)):
		var props = _parse_key(_keys[i])
		if props.branch == branch:
			_index = i
			break


func _parse_key(key) -> KeyProps:
	var props = KeyProps.new()
	var split = key.split("_")
	for s in split:
		if s[0] == "d":
			s.erase(0, 1)
			props.dialogue_id = int(s)
		if s[0] == "b":
			s.erase(0, 1)
			props.branch = int(s)
		if s[0] == "c" or s[0] == "r":
			props.type = s[0]
			s.erase(0, 1)
			props.id = int(s)
		if s[0] == "g":
			s.erase(0, 1)
			props.goto = int(s)
		if s == "m" or s == "f":
			props.gender = s
	return props


func _choice_mode():
	_choice = true
	for i in range(_index, len(_keys)):
		var props = _parse_key(_keys[i])
		if props.type != "r":
			_index = i
			break
		# Ignoring choices for the opposite gender
		if props.gender and props.gender != player.gender:
			continue
		_choices.append(_keys[i])
	for i in range(dialogue_choices.get_child_count()):
		dialogue_choices.get_child(i).visible = false
	dialogue_text.visible = false
	dialogue_choices.visible = true
	_update_choices_text()


func _choose():
	var key = _choices[_selected_choice]
	var props = _parse_key(key)
	_goto(props.goto)
	_choice = false
	_choices.clear()
	_selected_choice = 0
	dialogue_text.visible = true
	dialogue_choices.visible = false
	emit_signal("on_choice", key)


func _update_choices_text():
	for i in range(len(_choices)):
		var format = "%s"
		if i == _selected_choice:
			format = "[ %s ]"
		var choice = dialogue_choices.get_child(i)
		choice.visible = true
		choice.text = format % tr(_choices[i])


func _on_body_entered(body):
	if "player" in body.get_groups():
		# Dirty workaround, without it the signal is triggered prematurely
		if player.position.x < 100:
			return
		start_dialogue()


func _get_keys_from_loc_file(path, index):
	#print(path)
	var res = load(path)
	#print(res.get_message_count())
	var keys = res.get_message_list()
	var sorted = []
	
	for key in keys:
		var props = _parse_key(key)
		if props.dialogue_id != dialogue_index:
			continue
		sorted.append(key)
	
	sorted.sort()
	#sorted.sort_custom(self, "_sort_keys_branches")
	#sorted.sort_custom(self, "_sort_keys_types")

#	for key in sorted:
#		print(key)
#	print("----")
	
	return sorted


func _sort_keys_branches(a, b):
	var ap = _parse_key(a)
	var bp = _parse_key(b)
	if ap.branch < bp.branch:
		return true
	return false


func _sort_keys_types(a, b):
	var ap = _parse_key(a)
	var bp = _parse_key(b)
	if ap.branch == bp.branch:
		if ap.type == "r" and bp.type == "c":
			return false
	return true
