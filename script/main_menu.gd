extends Control


onready var game = get_node("/root/root")


var last_selected

var ignore_input = false

var _fade_signal_connected = false

func _ready():
	if name == "main":
		last_selected = get_node("buttons/play")
		if OS.get_name() == "HTML5":
			get_node("buttons/exit").visible = false
	else:
		last_selected = get_node("buttons/continue")
	if visible:
		focus()


func focus(ignore_input = false):
	last_selected.grab_focus()
	self.ignore_input = ignore_input
	if name == "pause":
		if game.camera.follow_object != game.player and \
			!game.camera.follow_object.get_node("..").skipped:
				get_node("buttons/skip").visible = true
		else:
			get_node("buttons/skip").visible = false


func _process(_delta):
	if not visible:
		return
	if Input.is_action_just_released("ui_accept"):
		ignore_input = false
	if Input.is_action_just_pressed("ui_accept") and not ignore_input:
		last_selected = get_focus_owner()
		_handle_button(last_selected.name)


func _handle_button(button):
	if button == "play":
		visible = false
		game.play()
	if button == "chapter":
		get_node("../chapters").focus()
		get_node("../chapters").visible = true
		visible = false
	if button == "continue":
		visible = false
		game.resume()
	if button == "skip":
		last_selected = get_node("buttons/continue")
		game.camera.follow_object.get_node("..").skip()
		_handle_button("continue")
	if button == "options":
		get_node("../options").focus()
		get_node("../options").visible = true
		visible = false
	if button == "credits":
		get_node("../credits").focus()
		get_node("../credits").visible = true
		visible = false
	if button == "go_to_main":
		last_selected = get_node("buttons/continue")
		visible = false
		if not _fade_signal_connected:
			game.camera.connect("on_fade_finish", self, "_on_fade_finish")
			_fade_signal_connected = true
		game.camera.fadein("go_to_main")
	if button == "exit":
		get_tree().quit()

func _on_fade_finish(name):
	if name == "go_to_main":
		get_node("../main").visible = true
		get_node("../main").focus()
		get_node("../dialogue").visible = false
		game.reset()
