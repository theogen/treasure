extends Node2D


signal on_unpause


export(String,
	"intro",
	"puzzle1",
	"puzzle2",
	"puzzle3",
	"monastery") \
var first_scene

onready var player = get_node("/root/root/player")
onready var camera = get_node("/root/root/camera")

var current_scene = null

onready var _main_menu = get_node("gui/main")
onready var _pause_menu = get_node("gui/pause")
onready var _options_menu = get_node("gui/options")
onready var _dialogue = get_node("gui/dialogue")

var _dialogue_text_visible = true
var _dialogue_choices_visible = false

var _game_ending = 1

var paused = false
var player_force_stop = false


func _ready():
	randomize()
	load_scene(first_scene)


func _a_menu_is_open():
	return get_node("gui/main").visible or \
		get_node("gui/pause").visible or \
		get_node("gui/options").visible or \
		get_node("gui/credits").visible


func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if not _a_menu_is_open():
			_pause()
		elif paused:
			if _options_menu.visible:
				_options_menu.back()
			else:
				_pause_menu.visible = false
				resume()


func _pause():
	_pause_menu.visible = true
	_pause_menu.focus()
	player.can_move = false
	paused = true
	if _dialogue.visible and _dialogue.margin_left == 0:
		_dialogue_text_visible = _dialogue.get_node("text").visible
		_dialogue.get_node("text").visible = false
		_dialogue_choices_visible = _dialogue.get_node("choices").visible
		_dialogue.get_node("choices").visible = false


func _unpause():
	paused = false
	if _dialogue.visible and _dialogue.margin_left == 0:
		_dialogue.get_node("text").visible = _dialogue_text_visible
		_dialogue.get_node("choices").visible = _dialogue_choices_visible
	emit_signal("on_unpause")


func play():
	#player.set_active(true)
	_unpause()
	camera.fadein("preface", false)


func start():
	player.set_active(true)
	_unpause()


func resume():
	_unpause()
	if not _dialogue.visible and not player_force_stop:
		player.can_move = true


func reset():
	_unpause()
	player.set_active(false)
	player_force_stop = false
	player.idle_name = "idle"
	load_scene(first_scene)


func load_scene(scene_name, display_name = false):
	player.position.x = player.startx
	camera.position.x = player.position.x
	camera.force_update_scroll()
	camera.reset_smoothing()
	if current_scene:
		remove_child(current_scene)
		current_scene.queue_free()
	var scene = load("res://scene/" + scene_name + ".tscn").instance()
	add_child(scene)
	current_scene = scene
	if display_name:
		get_node("gui/chapter").display("ui_chapter_" + scene_name)


func _on_dialogue_end(index):
	if index == 1:
		camera.fadeout()
		player.set_active(true)
		get_node("gui/chapter").display("ui_chapter_intro")
	if index == 3:
		get_node("monastery/door").play("open")
		get_node("monastery/door/sound").play()
		get_node("monastery/monks/guard1").play("point")
	if index == 4:
		player.can_move = false
		player_force_stop = true
		get_node("monastery/monks/guide1").connect("on_stop", self, "_on_monk_stop")
		get_node("monastery/monks/guide1").walk(860)
		get_node("monastery/monks/guide2").walk(860)
	if index == 5:
		player.can_move = false
		camera.fadein("epilogue", false)
	if index == 6 or index == 7:
		camera.fadeout()
		_ending_scene()
	if index == 8:
		player.can_move = false
		camera.fadein("end")


func _ending_scene():
	load_scene("intro")
	player.position = get_node("intro/grandpa").position
	player.can_move = false
	player.idle_name = "old"
	get_node("intro/grandpa").queue_free()
	get_node("intro/dialogue_zone").queue_free()
	camera.position.x = player.position.x
	camera.force_update_scroll()
	camera.reset_smoothing()
	var scene = load("res://prefab/fake_player_%s.tscn" % player.opposite_gender()).instance()
	get_node("intro").add_child(scene)
	get_node("intro/end_timer").connect("timeout", self, "_end")
	get_node("intro/parallax/mountains").visible = true


func _end():
	get_node("intro/dialogue_end").start_dialogue()


func _on_monk_stop():
	player.can_move = true
	player_force_stop = false
	get_node("monastery/monks/guide1").queue_free()
	get_node("monastery/monks/guide2").queue_free()


func _on_dialogue_choice(key):
	if key == "d1_b4_r1_g5":
		player.set_gender("f")
	if key == "d1_b4_r2_g6":
		player.set_gender("m")
	if "d5_b5_r1" in key:
		_game_ending = 1
	if "d5_b5_r2" in key:
		_game_ending = 2

func _on_dialogue_proceed(key):
	if key == "d4_b1_c3":
		get_node("monastery/monks/guide1").play("unhood")
		get_node("monastery/monks/guide2").play("unhood")
	if key == "d4_b1_c9":
		get_node("monastery/monks/guide1").play("point")
	if key == "d4_b1_c10":
		get_node("monastery/monks/guide1").play("idle")
	if key == "d5_b6_c1":
		get_node("treasury/monks/guide1").jump(true)
		get_node("treasury/monks/guide2").jump(true)
	if key == "d5_b6_c2":
		get_node("treasury/monks/guide1").jump(false)
		get_node("treasury/monks/guide2").jump(false)


func _on_camera_fade_finish(name):
	if name == "preface":
		get_node("preface").start_dialogue()
		get_node("intro/parallax/mountains").visible = true
	if name == "epilogue":
		get_node("epilogue" + str(_game_ending)).start_dialogue()
	if name == "end":
		get_node("gui/end").visible = true
		get_node("gui/end").focus(false)
		get_node("gui/main").last_selected = get_node("gui/main/buttons/play")
		reset()
