extends "res://script/platform_pillar.gd"

onready var _initial_y = position.y

func takedown():
	if position.y == -8:
		return
	move.y = -8
	_sound.play()

func buildup():
	if position.y == _initial_y:
		return
	move.y = _initial_y
	_sound.play()

func _enable_depending_on_player_level():
	pass
