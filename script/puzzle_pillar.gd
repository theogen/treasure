extends Node2D

onready var player = get_node("/root/root/player")

onready var skip_button = get_node("/root/root/gui/pause/buttons/skip")

var pillars = []
var pillar_reset

var skipped = false

func _ready():
	for child in get_children():
		if "platform" in child.get_groups():
			if "platform" in child.name:
				pillars.append(child)
				child.connect("on_platform_jump", self, "_on_platform_jump")
			elif child.name == "reset":
				pillar_reset = child
				pillar_reset.min_height = 0
				pillar_reset.max_height = 1
				child.connect("on_platform_land", self, "reset")
			elif "trigger" in child.name:
				child.connect("on_enter", self, "_trigger_all_pillars")

func skip():
	get_node("wall").takedown()
	skipped = true

func reset(_platform):
	if pillar_reset.height == 0:
		return
	pillar_reset.bury()
	for pillar in pillars:
		pillar.set_height(pillar.initial_height)
		pillar.swap_directions = false
	skipped = false
	get_node("wall").buildup()

func _on_platform_jump(platform):
	if platform.height == platform.min_height:
		return
	pillar_reset.elevate()
	platform.bury()
	#player.position.y += 8
	for pillar in pillars:
		if pillar != platform:
			_trigger_pillar(pillar)


func _trigger_pillar(pillar):
	if pillar.height == pillar.max_height:
		pillar.swap_directions = true
	if pillar.height == pillar.min_height:
		pillar.swap_directions = false
	if not pillar.swap_directions:
		pillar.elevate()
	else:
		pillar.bury()

func _trigger_all_pillars(_platform):
	pillar_reset.elevate()
	for pillar in pillars:
		_trigger_pillar(pillar)


func _on_Area2D_body_exited(body):
	pass # Replace with function body.
