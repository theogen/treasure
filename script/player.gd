extends KinematicBody2D

signal on_jump

var can_move = true

var snap = false

var run_speed = 50
var jump_speed = -140
var gravity = 400

var velocity = Vector2()

var startx

var sprite
var eyelids

var gender

var idle_name = "idle"

const JUMP_BUFFER = 0.3
var _jump_buffer = 0.0

func _ready():
	set_gender("f")
	eyelids = get_node("eyelids")
	blink(true)
	align_position_to_pixels()
	startx = position.x
	set_active(false)


func set_active(state):
	can_move = state
	visible = state


func opposite_gender():
	if gender == "m":
		return "f"
	else:
		return "m"


func set_gender(id):
	if id == "m":
		sprite = $guy_sprite
		$girl_sprite.visible = false
	else:
		sprite = $girl_sprite
		$guy_sprite.visible = false
	sprite.visible = true
	gender = id


func blink(state):
	if !state:
		eyelids.stop()
		get_node("eyelid").visible = false
		get_node("eyelid2").visible = false
	else:
		if gender == "m":
			eyelids.play("eyelids")
		else:
			eyelids.play("eyelid")


func align_position_to_pixels():
	position = Vector2(round(position.x), position.y)


func flip(state):
	transform.x = Vector2(-1 if state else 1, 0)


func get_input():
	if not can_move:
		velocity.x = 0
		sprite.play(idle_name)
		blink(true)
		align_position_to_pixels()
		return
	velocity.x = 0
	var right = Input.is_action_pressed('right')
	var left = Input.is_action_pressed('left')
	var jump = Input.is_action_just_pressed('jump')
	
	if not is_on_floor() and jump:
		_jump_buffer = JUMP_BUFFER
	
	if is_on_floor() and (jump or _jump_buffer > 0):
		velocity.y = jump_speed
		_jump_buffer = 0
		emit_signal("on_jump")
	
	if right:
		if is_on_floor():
			sprite.play("run")
		blink(false)
		flip(false)
		velocity.x += run_speed
	if left:
		if is_on_floor():
			sprite.play("run")
		blink(false)
		flip(true)
		velocity.x -= run_speed
	if !right and !left:
		if is_on_floor():
			sprite.play("idle")
		blink(true)
		flip(false)
		align_position_to_pixels()
	
	if !is_on_floor():
		if !right and !left:
			sprite.play("jump")
		blink(false)
	if right or left:
		blink(false)
		if !is_on_floor() and not "run_jump" in sprite.animation:
			if randi() % 2 == 0:
				sprite.play("run_jump1")
			else:
				sprite.play("run_jump2")


func _physics_process(delta):
	velocity.y += gravity * delta
	get_input()
	if not snap:
		velocity = move_and_slide(velocity, Vector2(0, -1))
	else:
		velocity = move_and_slide_with_snap(velocity, Vector2(0, 8), Vector2(0, -1))
	if _jump_buffer > 0:
		_jump_buffer -= delta
