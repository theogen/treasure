extends Node2D

export var distance = 100
export var sleep_distance = 150

onready var player = get_node("/root/root/player")
onready var camera = get_node("/root/root/camera")

func _ready():
	pass

func _process(_delta):
	var d = abs(player.global_position.x - global_position.x)
	if d > sleep_distance:
		return
	if d < distance:
		camera.follow_object = self
	else:
		camera.follow_object = camera.player
