extends Node2D


signal on_enter


onready var _sound = get_node("sound")


func _on_Area2D_body_entered(body):
	if not "player" in body.get_groups():
		return
	position.y += 2
	_sound.play()
	emit_signal("on_enter", self)


func _on_Area2D_body_exited(body):
	if not "player" in body.get_groups():
		return
	position.y -= 2
