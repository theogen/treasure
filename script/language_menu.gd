extends Control


func _ready():
	get_node("buttons/en").grab_focus()
	get_node("buttons/en").connect("focus_entered", self, "_button_en_focus_entered")
	get_node("buttons/ru").connect("focus_entered", self, "_button_ru_focus_entered")
	
func _process(_delta):
	if not visible:
		return
	if Input.is_action_just_pressed("ui_accept"):
		_handle_button(get_focus_owner())

func _handle_button(button):
	visible = false
	get_node("../main").visible = true
	get_node("../main").focus()

func _button_en_focus_entered():
	get_node("../options").set_language("en")

func _button_ru_focus_entered():
	get_node("../options").set_language("ru")
