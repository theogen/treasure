extends Label


export(String) var loc_prefix


var focused = false
onready var _select_sound = get_node("/root/root/gui/select_sound")


func _ready():
	update_text()
	connect("focus_entered", self, "_focus_entered")
	connect("focus_exited", self, "_focus_exited")
	get_node("/root/root/gui/options").connect("on_language_update", self, "update_text")


func get_text():
	return tr(loc_prefix + name)


func update_text():
	text = get_text()
	if focused:
		text = "[ %s ]" % text


func _focus_entered():
	focused = true
	_select_sound.play()
	update_text()


func _focus_exited():
	focused = false
	update_text()
