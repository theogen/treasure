extends Area2D


export(String) var level

onready var _root = get_node("/root/root")


func _ready():
	var err = connect("body_entered", self, "_body_entered")
	if err:
		print("Couldn't to connect body_entered signal!")
	_root.camera.limit_right = get_global_transform().origin.x - 18
	_root.camera.connect("on_fade_finish", self, "_on_fade_finish")


func _body_entered(body):
	if "player" in body.get_groups():
		print("Player has entered the portal, position: " 
			+ str(_root.player.position.x))
		# Dirty workaround, without it the signal is triggered prematurely
		if _root.player.position.x < 100:
			return
		_root.camera.fadein("portal")
		_root.player.can_move = false


func _on_fade_finish(fade_name):
	if fade_name != "portal":
		return
	print("Leaving " + get_node("..").name + ", entering " + level)
	_root.load_scene(level, true)
	_root.player.can_move = true
