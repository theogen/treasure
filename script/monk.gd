extends AnimatedSprite


signal on_stop


export(int, "Male", "Female") var gender = 0

const MOVEMENT_SPEED = 20

var hooded = false
var move
var _moving = false
var _jumping = false

func _ready():
	hooded = animation == "idle" or animation == "point"
	move = position


func _process(delta):
	position = position.move_toward(move, MOVEMENT_SPEED * delta)
	if _moving and position == move:
		_moving = false
		emit_signal("on_stop")

func walk(to_x):
	move.x = to_x
	if to_x < position.x:
		flip_h = true
	play("walk")
	_moving = true


func jump(state):
	_jumping = state
	if state:
		$AnimationPlayer.play("jump")


func play(anim = "default", backwards = false):
	var gender_name = "man"
	if gender == 1:
		gender_name = "woman"
	if (anim == "idle" or anim == "point") and hooded:
		.play(anim, backwards)
		return
	if anim == "idle" or anim == "walk" or anim == "point" or anim == "jump":
		.play("%s_%s" % [anim, gender_name], backwards)
	else:
		.play(anim, backwards)


func _on_animation_finished():
	if animation == "unhood":
		hooded = false
		play("idle")


func _jump_animation_callbacks(state):
	if state == "jumped":
		play("jump")
	if state == "landed":
		play("idle")
		if not _jumping:
			$AnimationPlayer.stop()
