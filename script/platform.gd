extends Node2D

signal on_platform_jump
signal on_platform_land
signal on_platform_exit

var playerInside = false
var playerJumpedOnPlatform = false

onready var player = get_node("/root/root/player")


func _ready():
	player.connect("on_jump", self, "_on_player_jump")


func _on_player_jump():
	playerJumpedOnPlatform = false
	if playerInside:
		playerJumpedOnPlatform = true


func _on_Area2D_body_entered(body):
	var groups = body.get_groups()
	if "player" in groups:
		playerInside = true
		emit_signal("on_platform_land", self)
		if playerJumpedOnPlatform:
			emit_signal("on_platform_jump", self)
			playerJumpedOnPlatform = false


func _on_Area2D_body_exited(body):
	var groups = body.get_groups()
	if "player" in groups:
		playerInside = false
		emit_signal("on_platform_exit")
