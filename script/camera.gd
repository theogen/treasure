extends Camera2D


signal on_fade_finish


onready var player = get_node("/root/root/player")

onready var _fade = get_node("fade")

var follow_object

var _fade_name
var _fadeout_automatically


func _ready():
	follow_object = player


func _process(_delta):
	if not follow_object.is_inside_tree():
		follow_object = player
	global_position.x = follow_object.global_position.x


func fadein(name, fadeout_automatically = true):
	_fade.play("fadein")
	_fade_name = name
	_fadeout_automatically = fadeout_automatically


func fadeout():
	_fade.play("fadeout")


func _on_fade_animation_finished(anim_name):
	if not anim_name == "fadein":
		return
	emit_signal("on_fade_finish", _fade_name)
	if _fadeout_automatically:
		_fade.play("fadeout")
