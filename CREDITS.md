# CREDITS

Developed by Theogen Ratin.

- [Song "Omega Sagittarii" by Coldest Winter](https://soundcloud.com/coldest-winter-177974924/omega-sagittarii)

- [Ocean sound by InspectorJ](https://freesound.org/people/InspectorJ/sounds/400632/)

- [Forest sound by klankbeeld](https://freesound.org/people/klankbeeld/sounds/532424/)

- [Wind sound #1 by kyles](https://freesound.org/people/kyles/sounds/454360/)

- [Wind sound #2 by janbezouska](https://freesound.org/people/janbezouska/sounds/397091/)

- [Dialogue printing sound by bay\_area\_bob](https://freesound.org/people/bay_area_bob/sounds/541987/)

- Pillar sound is mixed from from:

	- ["concrete blocks moving2.wav" by FreqMan](https://freesound.org/people/FreqMan/sounds/25846/)

	- ["Rockfall in mine.wav" by Benboncan](https://freesound.org/people/Benboncan/sounds/60085/)

	- ["Door, Wooden, Close, A (H6 MS).wav" by InspectorJ](https://freesound.org/people/InspectorJ/sounds/411789/)

- [Door sound by rambler52](https://freesound.org/people/rambler52/sounds/455321/)
