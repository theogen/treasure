# TODO

## Due Saturday

- Visuals
	- Campfires
	- Reflection
	- Birds
	- Torches

## Done

- Cat
X Savefiles
X Avatars
- Jump buffer
- Translate to Russian
- Disable exit on web
- Audio, music
- Nighttime
- Parralax background
- Chapter names
- Language selector
- Option to skip puzzles
- Story
- Characters
- Dialogues
- Main menu
- Chapter menu
- Distinct locations
- Puzzles
- Pillar animations
- Scene management
- Platforms
- Monastery
- Blinking
- Stop at even pixels
- Jumping animations
- Walking animations
- Keep aspect ratio
- Camera follow
